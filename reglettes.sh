#!/bin/sh
#
# avec un fichier urls.txt qui contiendrait ces données:
#
# lilypie http://lmtf.lilypie.com/fQqDp2.png
# enceintepointcom http://www.enceinte.com/reglette-349451.png
# thebump http://global.thebump.com/tickers/tt1c5186.aspx
# thebump-2 http://global.thebump.com/tickers/tt1a87b2.aspx
# babygaga http://tickers.babygaga.com/p/dev025pr___.png
# pregnology http://www.pregnology.com/preggoticker2/777777/000000/My%20pregnancy/01/25/2016.png
# neufmois http://www.neufmois.fr/les-tickers-suivi-de-grossesse/84110.png
#
# tu lances le script:
# reglettes.sh < urls.txt
# ou alors comme ça :
# cat urls.txt | reglettes.sh
#
# INTÉRÊT : en fait urls.txt peut venir d'ailleurs (une requête de db, etc, que tu peux piper au script)

date=$(date +%Y-%m-%d)
branch="crontab/${date}"

git checkout -q -b "$branch"

while read dir url ; do
  target="images/$dir/${date}.png"
  #echo "$url"
  #echo "$target"
  if [ ! -f "$target" -o ! -s "$target" ]
    then
    /usr/local/bin/wget "$url" -O "$target"
    git add images
    git commit -m "${target} created from ${url}"
  fi
  #echo "-------"
done

# je ferais bien un fetch de l'upstream avant ?
git checkout -q master
git merge -q --no-ff --no-edit $branch
git branch -q -d $branch
git push -q origin

